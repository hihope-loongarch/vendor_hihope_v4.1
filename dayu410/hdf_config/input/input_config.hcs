root {
    input_config {
        touchConfig {
            touch0 {
                boardConfig {
                    match_attr = "touch_device1";
                    inputAttr {
                        /* 0:touch 1:key 2:keyboard 3:mouse 4:button 5:crown 6:encoder */
                        inputType = 0;
                        solutionX = 600;
                        solutionY = 1024;
                        devName = "main_touch";
                    }

                    // TP config for LOONGSON LS2K1000_DEV_BOARD_V1.0
                    busConfig {
                        // 0:i2c 1:spi
                        busType = 0;
                        busNum = 1; // I2C1
                        clkGpio = 18;  // GPIO18 to I2C1_SCL
                        dataGpio = 19; // GPIO19 to I2C1_SDA
                        i2cClkIomux = [0x1fe00420, 0x2FF60207, 0x00000409];
                        i2cDataIomux = [0x1fe00420, 0x2FF60207, 0x00000409];
                    }

                    pinConfig {
                        rstGpio = 0;  // NC
                        intGpio = 0;  // GPIO0 to CT_INT
                        rstRegCfg = [0x1fe00530, 0x00000000, 0x00000001];
                        intRegCfg = [0x1fe00530, 0x00000000, 0x00000001];
                    }

                    powerConfig {
                        /* 0:unused 1:ldo 2:gpio 3:pmic */
                        vccType = 2;
                        vccNum = 20;    // gpio20
                        vccValue = 1800;
                        vciType = 1;
                        vciNum = 12;    // ldo12
                        vciValue = 3300;
                    }

                    featureConfig {
                        capacitanceTest = 0;
                        gestureMode = 0;
                        gloverMOde = 0;
                        coverMode = 0;
                        chargerMode = 0;
                        knuckleMode = 0;
                    }
                }

                chipConfig {
                    template touchChip {
                        match_attr = "";
                        chipName = "gt911";
                        vendorName = "zsj";
                        chipInfo = "AAAA11222";  // 4-ProjectName, 2-TP IC, 3-TP Module
                        /* 0:i2c 1:spi*/
                        busType = 0;
                        deviceAddr = 0x5D;
                        /* 0:None 1:Rising 2:Failing 4:High-level 8:Low-level */
                        irqFlag = 2;
                        maxSpeed = 400;
                        chipVersion = 0; //parse Coord TypeA
                        powerSequence {
                            /* [type, status, dir , delay]
                                <type> 0:none 1:vcc-1.8v 2:vci-3.3v 3:reset 4:int
                                <status> 0:off or low  1:on or high  2:no ops
                                <dir> 0:input  1:output  2:no ops
                                <delay> meanings delay xms, 20: delay 20ms
                             */
                            powerOnSeq = [4, 0, 1, 0,
                                         3, 0, 1, 10,
                                         3, 1, 2, 60,
                                         4, 2, 0, 0];
                            suspendSeq = [3, 0, 2, 10];
                            resumeSeq = [3, 1, 2, 10];
                            powerOffSeq = [3, 0, 2, 10,
                                           1, 0, 2, 20];
                        }
                    }

                    chip0 :: touchChip {
                        match_attr = "zsj_gt911_5p5";
                        chipInfo = "ZIDN45100";  // 4-ProjectName, 2-TP IC, 3-TP Module
                        chipVersion = 0; //parse point by TypeA
                    }

                    chip1 :: touchChip {
                        match_attr = "zsj_gt911_4p0";
                        chipInfo = "ZIDN45101";
                        chipVersion = 1; //parse point by TypeB
                    }

                    chip2 :: touchChip {
                        match_attr = "tg_gt911_7p0";
                        vendorName = "tg";
                        chipInfo = "ZIDN45102";
                        chipVersion = 2; //parse point by TypeC
                    }

                    chip3 :: touchChip {
                        match_attr = "st_ft6336_2p35";
                        chipName = "ft6336";
                        vendorName = "st";
                        chipInfo = "ZIDN53200";
                        deviceAddr = 0x38;
                        irqFlag = 1; // Rising
                        powerSequence {
                            /* [type, status, dir , delay]
                                <type> 0:none 1:vcc-1.8v 2:vci-3.3v 3:reset 4:int
                                <status> 0:off or low  1:on or high  2:no ops
                                <dir> 0:input  1:output  2:no ops
                                <delay> meanings delay xms, 20: delay 20ms
                             */
                            powerOnSeq = [4, 2, 0, 0,
                                         3, 1, 1, 2,
                                         3, 0, 2, 5,
                                         3, 1, 2, 200];
                            suspendSeq = [3, 0, 2, 10];
                            resumeSeq = [3, 1, 2, 10];
                            powerOffSeq = [3, 0, 2, 10,
                                           1, 0, 2, 20];
                        }
                    }
                }
            }
        }

        keyConfig {
            keyList = ["power", "VolUp", "VolDown", "Up", "Down", "Left", "Right"];
            keyInfoList {
                key1 {
                    match_attr = "key_device0";
                    /* 0:touch 1:key 2:keyboard 3:mouse 4:button 5:crown 6:encoder */
                    inputType = 1;
                    keyName = "power";
                    gpioNum = 1;
                    irqFlag = 3;
                    debounceTime = 80;
                }

                key2 {
                    keyName = "volUp";
                    gpioNum = 31;
                    irqFlag = 1;
                    debounceTime = 80;
                }

                key3 {
                    keyName = "volDown";
                    gpioNum = 32;
                    irqFlag = 1;
                    debounceTime = 80;
                }
            }
        }
    }
}
