/*
 * Copyright (c) 2023 Loongson(GD) Inc.
 * Author: loongson-gz
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#ifndef __LSOH_NVMEM_IO_I_H__
#define __LSOH_NVMEM_IO_I_H__

/*
 * 这份文件是内部运作的接口
 * 见 mtd
 * 需要提供 nv_var_field 的数组 ls_nv_var_set，记录的是单个要记录变量在存储区的偏移和大小
 * 需要提供 nv_var_io_func_set 的实例 ls_nv_var_io_func_set，声明接口，里面的函数是具体调用的mtd读写的函数。
 */

typedef struct nv_var_field {
	const char* location;
	int size;
	unsigned long offset;
} nv_var_field;

typedef int (*read_nvme_var)(nv_var_field* info, unsigned char* package, int package_size);
typedef int (*write_nvme_var)(nv_var_field* info, unsigned char* package, int package_size);

typedef struct nv_var_io_func_set{
	read_nvme_var read_var;
	write_nvme_var write_var;
} nv_var_io_func_set;

#endif