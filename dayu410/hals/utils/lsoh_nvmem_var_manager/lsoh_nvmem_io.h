/*
 * Copyright (c) 2023 Loongson(GD) Inc.
 * Author: loongson-gz
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#ifndef __LSOH_NVMEM_IO_H__
#define __LSOH_NVMEM_IO_H__

typedef enum nv_var_type {
	NV_TYPE_SN = 0,
	NV_TYPE_TOKEN,
	NV_TYPE_MAX,
} nv_var_type;

/*
 * nv_var_read 和 nv_var_write 是读写非易失存储的函数
 * 由于读写的内容的大小是固定的，所以需要先 get_nv_var_size 知道读写的大小
 * 这样才知道调用完 nv_var_read 后，buf 申请了空间有多大 (通常是 + 1，应对字符串显示)。
 * 才知道调用 nv_var_write buf的本来空间有多大。
 * 举个例子
 * SN 预留了64字节存储
 * get_nv_var_size(NV_TYPE_SN) 得到 64。
 * 调用 nv_var_read 时，传入的 buf 需要是一个空指针的地址
 * 下面是例子
 *
 * unsigned char* tmp = NULL;
 * nv_var_read(NV_TYPE_SN, &tmp);
 * // do something
 * // tmp size is 65 and tmp[64] == 0;
 * free(tmp);
 * tmp = NULL;
 *
 * 调用 nv_var_write 时，传入的 buf 的空间大小需要是64。不用考虑字符串的最后一个\0
 * 下面是例子
 *
 * unsigned char* tmp = NULL;
 * int len = get_nv_var_size(NV_TYPE_SN);
 * tmp = (unsigned char*)calloc(len, sizeof(unsigned char));
 * memcpy(tmp, "demo-0001", 9); // demo-0001 是9字节
 * nv_var_write(NV_TYPE_SN, tmp);
 * free(tmp);
 * tmp = NULL;
 */

int nv_var_read(nv_var_type var_type, unsigned char** buf);
int nv_var_write(nv_var_type var_type, unsigned char* buf);
int get_nv_var_size(nv_var_type var_type);

#endif