/*
 * Copyright (c) 2023 Loongson(GD) Inc.
 * Author: loongson-gz
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#ifndef __LSOH_NVMEM_MTD_IO_H__
#define __LSOH_NVMEM_MTD_IO_H__

#include "lsoh_nvmem_io_i.h"

#define LSOH_MTD_LOCATION "bdinfo"

int lsoh_nvmem_mtd_read_var(nv_var_field* info, unsigned char* package, int package_size);
int lsoh_nvmem_mtd_write_var(nv_var_field* info, unsigned char* package, int package_size);

#endif