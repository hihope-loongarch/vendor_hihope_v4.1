/*
 * Copyright (c) 2023 Loongson(GD) Inc.
 * Author: loongson-gz
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#ifndef _MTD_IO_H_
#define _MTD_IO_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <string.h>

//mtd分区的信息
typedef struct mtd_info
{
	int mtd_num; //mtd 的 编号
	int mtd_fd;  //mtd 文件描述符
	char* mtd_dev_name;  //mtdb设备文件路径
	int mtdblock_fd;  //mtd 文件描述符
	char* mtdblock_dev_name;  //mtdblock设备文件路径

	unsigned long size;
	unsigned long erase_size;
	int write_size;
} mtd_info;

int open_mtd(char* mtd_name, mtd_info** info);
int close_mtd(mtd_info** info);

int read_mtdblock(mtd_info* info, unsigned char* data, unsigned long data_size, unsigned long offset);
int write_mtdblock(mtd_info* info, unsigned char* data, unsigned long data_size, unsigned long offset);

#endif