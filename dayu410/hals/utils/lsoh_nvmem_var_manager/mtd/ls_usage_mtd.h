/*
 * Copyright (c) 2023 Loongson(GD) Inc.
 * Author: loongson-gz
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#ifndef _MTD_USAGE_H_
#define _MTD_USAGE_H_

// ls-device-usage-interface-start
int read_mtd(char* mtd_name, unsigned char* data, unsigned long data_size, unsigned long offset);
int write_mtd(char* mtd_name, unsigned char* data, unsigned long data_size, unsigned long offset);
int read_mtd_to_file(char* mtd_name, char* file_path, unsigned long data_size, unsigned long offset);
int write_mtd_by_file(char* mtd_name, char* file_path, unsigned long data_size, unsigned long offset);
// ls-device-usage-interface-end

#endif