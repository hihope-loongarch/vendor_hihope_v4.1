/*
 * Copyright (c) 2023 Loongson(GD) Inc.
 * Author: loongson-gz
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 */

#include "lsoh_nvmem_mtd_io.h"
#include "ls_usage_mtd.h"

nv_var_field ls_nv_var_set[] = {
	{ // SN
		.location = LSOH_MTD_LOCATION,
		.size = 64,
		.offset = 1024,
	},
	{ // token
		.location = LSOH_MTD_LOCATION,
		.size = 151,
		.offset = 2048,
	},
};

nv_var_io_func_set ls_nv_var_io_func_set = {
	.read_var = lsoh_nvmem_mtd_read_var,
	.write_var = lsoh_nvmem_mtd_write_var,
};

int lsoh_nvmem_mtd_read_var(nv_var_field* info, unsigned char* package, int package_size)
{
	return read_mtd((char*)info->location, package, package_size, info->offset);
}

int lsoh_nvmem_mtd_write_var(nv_var_field* info, unsigned char* package, int package_size)
{
	return write_mtd((char*)info->location, package, package_size, info->offset);
}